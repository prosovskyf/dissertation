
const Router = require('koa-router');
const router = Router({ prefix: '/api/v1/test' });

router.get('/', getTest);

async function getTest(ctx) {
    ctx.status = 200;
    ctx.body = 'ready';
}

module.exports = router;