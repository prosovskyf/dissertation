
const Koa = require('koa');
const test = require('./routes/proofRoute')

const app = new Koa();
app.use(test.routes());

module.exports = app;