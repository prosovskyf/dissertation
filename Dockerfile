FROM node:14

# Create app directory
WORKDIR /usr/src/app

# copy package.json and lock file
COPY package.json ./

# install dependecies needed (for sharp package) and run npm install
RUN apt-get update && apt-get install -y build-essential && npm install

# copy all other files
COPY . .

# expose ports (3005 API)
EXPOSE 3005

# run fileserver, api and docs at the same time
CMD [ "npm", "start" ]
